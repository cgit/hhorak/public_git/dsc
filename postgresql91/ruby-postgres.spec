%{?scl:%scl_package ruby-postgres}
%{!?scl:%global pkg_name ruby-postgres}
%{!?ruby_sitearch: %define ruby_sitearch %{?_scl_root}%(ruby -rrbconfig -e "puts Config::CONFIG['sitearchdir']")}

Name: %{?scl_prefix}ruby-postgres
Version: 0.7.9
Release: 2010.01.28.2%{?dist}
Summary: A Ruby interface for the PostgreSQL database engine
Group: Development/Languages
# Source says that ruby-gems-postgres is distributable under the same
# terms as ruby.
License: Ruby
URL: http://bitbucket.org/ged/ruby-pg/
Source: http://bitbucket.org/ged/ruby-pg/downloads/%{pkg_name}-%{version}.2008.01.28.tar.gz
Patch1: 0001-Implement-trace.patch
Patch2: 0002-Implement-notifies.patch
Patch3: 0003-Implement-print.patch

Requires: ruby(abi) = 1.8

BuildRequires: %{?scl_prefix}postgresql-devel >= 6.5
BuildRequires: ruby-devel

Provides: %{?scl_prefix}ruby(postgres) = %{version}

%{?scl:Requires:%scl_runtime}

%description
Database driver to access PostgreSQL databases from Ruby.

%prep
%setup -q -n %{pkg_name}-%{version}
%patch1 -p1
%patch2 -p1
%patch3 -p1
chmod a-x sample/psql.rb

%build
%{?scl:scl enable %scl - << \EOF}
cd ext
ruby extconf.rb --with-cflags="$RPM_OPT_FLAGS"
make
%{?scl:EOF}

%install
cd ext
make DESTDIR=%{buildroot}%{?scl:%_scl_root} install

%files
%defattr(-, root, root)
%doc ChangeLog README README.ja doc sample
%{ruby_sitearch}/postgres.so

%changelog
* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.9-2010.01.28.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Wed Feb 09 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.9-2009.01.28.2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Sep 08 2010 Chris Lalancette <clalance@redhat.com> - 0.7.9.2008.01.28.2
- Add patch to implement trace support (from upstream ruby-pg project)
- Add patch to implement print support (from older 0.7.1 sources)
- Add patch to implement notifies call (from older 0.7.1 sources)

* Fri Aug 13 2010 Chris Lalancette <clalance@redhat.com> - 0.7.9.2008.01.28.1
- Update to ruby-postgres .7.9.2008.01.28

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.1-12
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.7.1-11
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Wed Sep  3 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.7.1-10
- apply patch properly

* Wed Sep  3 2008 Tom "spot" Callaway <tcallawa@redhat.com> - 0.7.1-9
- fix license tag

* Tue Feb 19 2008 Fedora Release Engineering <rel-eng@fedoraproject.org> - 0.7.1-8
- Autorebuild for GCC 4.3

* Wed Aug 22 2007 David Lutterkort <dlutter@redhat.com> - 0.7.1-7
- Rebuild for F8 buildid

* Fri Dec  8 2006 David Lutterkort <dlutter@redhat.com> - 0.7.1-6
- Rebuild against PostgreSQL 8.2.0

* Wed Sep 13 2006 David Lutterkort <dlutter@redhat.com> - 0.7.1-5
- Rebuilt for FC6
- Remove _smp_mflags from make install

* Wed Aug  2 2006 David Lutterkort <dlutter@redhat.com> - 0.7.1-4
- Fix CFLAGS
- Add id to buildroot path

* Tue Jul 11 2006 David Lutterkort <dlutter@redhat.com> - 0.7.1-3
- Bring in accordance with packaging guidelines
- Changed license from 'Distributable' to 'Ruby License'

* Mon Jun  5 2006 David Lutterkort <dlutter@redhat.com> - 0.7.1-2
- Clean build root for install

* Wed May 24 2006 David Lutterkort <dlutter@redhat.com> - 0.7.1-1
- Initial build

