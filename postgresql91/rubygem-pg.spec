%{?scl:%scl_package rubygem-%{gemname}}
%{!?scl:%global pkg_name %{name}}

# Generated from pg-0.11.0.gem by gem2rpm -*- rpm-spec -*-
%global gemdir %{?_scl_root}%(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)
%global gemname pg
%global geminstdir %{gemdir}/gems/%{gemname}-%{version}
%{!?ruby_sitearch: %global ruby_sitearch %{?_scl_root}%(ruby -rrbconfig -e 'puts Config::CONFIG["sitearchdir"]')}
%global rubyabi 1.8

Summary: A Ruby interface to the PostgreSQL RDBMS
Name: %{?scl_prefix}rubygem-%{gemname}
Version: 0.12.2
Release: 2%{?dist}
Group: Development/Languages
# Upstream license clarification (https://bitbucket.org/ged/ruby-pg/issue/72/)
#
# The portions of the code that are BSD-licensed are licensed under
# the BSD 3-Clause license; the contents of the BSD file are incorrect.
#
License: (GPLv2 or Ruby) and BSD
URL: http://bitbucket.org/ged/ruby-pg/
Source0: http://rubygems.org/gems/%{gemname}-%{version}.gem
Requires: ruby(abi) = %{rubyabi}
Requires: rubygems
Requires: ruby
BuildRequires: ruby(abi) = %{rubyabi}
BuildRequires: rubygems
BuildRequires: ruby ruby-devel
BuildRequires: %{?scl_prefix}postgresql-server %{?scl_prefix}postgresql-devel
# EPEL does not have RSpec 2.x :/
%if 0%{?fedora}
# Keep requiring rspec-core as long as rubygem(rspec) is provided by RSpec 1.x
BuildRequires: rubygem(rspec-core)
%endif
Provides: rubygem(%{gemname}) = %{version}

%description
This is the extension library to access a PostgreSQL database from Ruby.
This library works with PostgreSQL 7.4 and later.


%package doc
Summary: Documentation for %{pkg_name}
Group: Documentation
Requires: %{?scl_prefix}%{pkg_name} = %{version}-%{release}

%description doc
Documentation for %{pkg_name}


%prep
%setup -n %{pkg_name}-%{version} -q -c -T
mkdir -p .%{gemdir}
export CONFIGURE_ARGS="--with-cflags='%{optflags}'"
%{?scl:scl enable %{scl} "}
gem install --local --install-dir .%{gemdir} \
            -V --force %{SOURCE0}
%{?scl:"}

%build

%install
mkdir -p %{buildroot}%{gemdir}
cp -a .%{gemdir}/* \
        %{buildroot}%{gemdir}/

install -d -m0755 %{buildroot}%{ruby_sitearch}
mv %{buildroot}%{geminstdir}/lib/pg_ext.so %{buildroot}%{ruby_sitearch}/

# Remove the binary extension sources and build leftovers.
rm -rf %{buildroot}%{geminstdir}/ext

# Remove useless shebangs.
sed -i -e '/^#!\/usr\/bin\/env/d' %{buildroot}%{geminstdir}/lib/pg.rb
sed -i -e '/^#!rake/d' %{buildroot}%{geminstdir}/Rakefile
sed -i -e '/^#!rake/d' %{buildroot}%{geminstdir}/Rakefile.cross

# Fix spec shebangs.
# https://bitbucket.org/ged/ruby-pg/issue/74/
for file in `find %{buildroot}%{geminstdir}/spec -type f ! -perm /a+x -name "*.rb"`; do
    [ ! -z "`head -n 1 $file | grep \"^#!/\"`" ] \
        && sed -i -e 's/^#!\/usr\/bin\/env spec/#!\/usr\/bin\/env rspec/' $file \
        && chmod -v 755 $file
done

%if 0%{?fedora}
%check
pushd .%{geminstdir}
%{?scl:scl enable %{scl} "}
rspec spec
%{?scl:"}
popd
%endif

%files
%exclude %{geminstdir}/.gemtest
%{ruby_sitearch}/pg_ext.so
%dir %{geminstdir}
%doc %{geminstdir}/BSD
%doc %{geminstdir}/GPL
%doc %{geminstdir}/LICENSE
%{geminstdir}/lib
%{gemdir}/cache/%{gemname}-%{version}.gem
%{gemdir}/specifications/%{gemname}-%{version}.gemspec

%files doc
%doc %{gemdir}/doc/%{gemname}-%{version}
%doc %{geminstdir}/ChangeLog
%doc %{geminstdir}/Contributors.rdoc
%doc %{geminstdir}/History.rdoc
%doc %{geminstdir}/Manifest.txt
%{geminstdir}/Rakefile
%{geminstdir}/Rakefile.cross
%doc %{geminstdir}/README.rdoc
%doc %{geminstdir}/README.ja.rdoc
%doc %{geminstdir}/README.OS_X.rdoc
%doc %{geminstdir}/README.windows.rdoc
%{geminstdir}/misc
%{geminstdir}/sample
%{geminstdir}/spec

%changelog
* Tue Apr 24 2012 Vít Ondruch <vondruch@redhat.com> - 0.12.2-2
- Fix provides macro.

* Tue Apr 03 2012 Vít Ondruch <vondruch@redhat.com> - 0.12.2-1
- Upgrade to pg 0.12.2.

* Sat Jan 14 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.11.0-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Fri Jun 03 2011 Vít Ondruch <vondruch@redhat.com> - 0.11.0-5
- Pass CFLAGS to extconf.rb.

* Fri Jun 03 2011 Vít Ondruch <vondruch@redhat.com> - 0.11.0-4
- Binary extension moved into ruby_sitearch dir.
- -doc subpackage made architecture independent.

* Wed Jun 01 2011 Vít Ondruch <vondruch@redhat.com> - 0.11.0-3
- Quoted upstream license clarification.

* Mon May 30 2011 Vít Ondruch <vondruch@redhat.com> - 0.11.0-2
- Removed/fixed shebang in non-executables.
- Removed sources.

* Thu May 26 2011 Vít Ondruch <vondruch@redhat.com> - 0.11.0-1
- Initial package
