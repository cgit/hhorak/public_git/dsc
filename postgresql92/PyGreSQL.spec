# Sets %%pkg_name to 'PyGreSQL' if we build for scl.
%{?scl:%scl_package PyGreSQL}
# If we don't build for scl, then pkg_name is empty.
# This way, we can always use %%pkg_name for 'PyGreSQL', be it scl build or not.
%{!?scl:%global pkg_name PyGreSQL}
# %%{name} then references to either 'PyGreSQL' (not in scl) or '%%{?scl:%scl_prefix}PyGreSQL' (in scl).

Name:		%{?scl_prefix}PyGreSQL
Version:	4.0
Release:	4%{?dist}
Summary:	A Python client library for PostgreSQL

Group:		Applications/Databases
URL:		http://www.pygresql.org/
# Author states his intention is to dual license under PostgreSQL or Python
# licenses --- this is not too clear from the current tarball documentation,
# but hopefully will be clearer in future releases.
# The PostgreSQL license is very similar to other MIT licenses, but the OSI
# recognizes it as an independent license, so we do as well.
License:	PostgreSQL or Python

Source0:	ftp://ftp.pygresql.org/pub/distrib/PyGreSQL-%{version}.tgz

Patch1:		PyGreSQL-set-decimal.patch

# PyGreSQL was originally shipped as a sub-RPM of the PostgreSQL package;
# these Provides/Obsoletes give a migration path.  Note there is no
# intention of changing the version numbers in future.
Provides:	%{?scl_prefix}postgresql-python = 8.5.0-1
Obsoletes:	%{?scl_prefix}postgresql-python < 8.5

BuildRequires:	%{?scl_prefix}postgresql-devel python-devel

%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

%description
PostgreSQL is an advanced Object-Relational database management system.
The PyGreSQL package provides a module for developers to use when writing
Python code for accessing a PostgreSQL database.

%prep
%setup -q -n %{pkg_name}-%{version}

%patch1 -p1

# Some versions of PyGreSQL.tgz contain wrong file permissions
chmod 755 tutorial
chmod 644 tutorial/*.py

%build
%{?scl:scl enable %scl - << \EOF}
CFLAGS="%{optflags}" %{__python} setup.py build
%{?scl:EOF}

%install
%{?scl:scl enable %scl - << \EOF}
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT%{?scl:%_scl_root}
%{?scl:EOF}

%files
%defattr(-,root,root,-)
%doc docs/*.txt
%doc tutorial
%{?scl:%_scl_root}%{python_sitearch}/*.so
%{?scl:%_scl_root}%{python_sitearch}/*.py
%{?scl:%_scl_root}%{python_sitearch}/*.pyc
%{?scl:%_scl_root}%{python_sitearch}/*.pyo
%{?scl:%_scl_root}%{python_sitearch}/*.egg-info

%changelog
* Thu Jan 12 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Thu Jul  7 2011 Tom Lane <tgl@redhat.com> 4.0-3
- Add upstream patch for set_decimal bug
Resolves: #719093

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 4.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Wed Dec 29 2010 Tom Lane <tgl@redhat.com> 4.0-1
- Update to PyGreSQL 4.0
- Relabel license as PostgreSQL now that that's separately recognized by OSI.

* Wed Jul 21 2010 David Malcolm <dmalcolm@redhat.com> - 3.8.1-3
- Rebuilt for https://fedoraproject.org/wiki/Features/Python_2.7/MassRebuild

* Tue Nov 24 2009 Tom Lane <tgl@redhat.com> 3.8.1-2
- Fix License tag and permissions on example scripts under tutorial/,
  per discussion in package review request.
Related: #452321

* Fri Jun 20 2008 Tom Lane <tgl@redhat.com> 3.8.1-1
- Created package by stripping down postgresql specfile and adjusting
  to meet current packaging guidelines for python modules.
