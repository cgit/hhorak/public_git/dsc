%{!?scl:%global scl unixodbc-2.3}
%scl_package %scl

Summary: Package that installs %scl
Name: %scl_name
Version: 1
Release: 1%{?dist}
BuildArch: noarch
License: GPLv2+
Requires: scl-utils
Requires: %{scl_prefix}unixODBC
BuildRequires: scl-utils-build

%description
This is the main package for %scl scl.

%package runtime
Summary: Package that enables %scl scl
Requires: %{name} = %{version}-%{release}

%description runtime
Package shipping essential scripts to work with %scl scl.

%package build
Summary: Package shipping basic build configuration

%description build
Package shipping essential configuration macros to build %scl scl.

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_scl_scripts}/root

# During the build of this package, we don't know which architecture it is 
# going to be used on, so if we build on 64-bit system and use it on 32-bit, 
# the %{_libdir} would stay expanded to '.../lib64'. This way we determine 
# architecture everytime the 'scl enable ...' is run and set the 
# LD_LIBRARY_PATH accordingly
cat >> %{buildroot}%{_scl_scripts}/enable << \EOF
export PATH=%{_bindir}:$PATH
export CPATH=%{_includedir}:$CPATH
export LIBRARY_PATH=%{_scl_root}`rpm -E %%_libdir`:$LIBRARY_PATH
export LD_LIBRARY_PATH=%{_scl_root}`rpm -E %%_libdir`:$LD_LIBRARY_PATH
EOF
%scl_install

%files

%files runtime
%scl_files

%files build
%{_root_sysconfdir}/rpm/macros.%{scl}-config

%changelog
* Wed Nov 30 2011 Honza Horak <hhorak@redhat.com> - 1-1
- Initial package
